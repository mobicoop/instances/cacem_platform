export default {
  // Uncomment to enable dark theme
  //dark: true,
  themes: {
    light: {
      primary: '#0177bf',
      accent: '#6cb430',
      secondary: '#E3640A',
      success: '#6cb430',
      info: '#000000',
      warning: '#E3640A',
      error: '#F03C0E'
    },
    dark: {
      primary: '#0177bf',
      accent: '#6cb430',
      secondary: '#E3640A',
      success: '#6cb430',
      info: '#000000',
      warning: '#E3640A',
      error: '#F03C0E'
    }
  }
}
